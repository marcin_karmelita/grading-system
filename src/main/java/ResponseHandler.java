import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by marcinkarmelita on 27/04/17.
 */
public interface ResponseHandler {
    Response notFoundResponse(Object object);
    Response inconsistentDataResponse(Object object);
    Response okResponse(Object object);
    Response createdResponse(Object object);
}

