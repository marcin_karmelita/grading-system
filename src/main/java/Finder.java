import javax.ws.rs.core.Response;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by marcinkarmelita on 27/04/17.
 */
public interface Finder<T> {
    Response find(List<T> list, Predicate<T> predicate);
}
