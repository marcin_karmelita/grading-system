import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by marcinkarmelita on 27/04/17.
 */
public class GradeValidationHandler implements ValidationHandler<Grade> {

    @Override
    public Response.Status validateContent(Grade object) {
        boolean not_expected = ValidationHandler.exists(object.getCourseId()) ||
                ValidationHandler.exists(object.getGradeId()) ||
                ValidationHandler.exists(object.getStudentIndex());
        if (not_expected) {
            return Response.Status.EXPECTATION_FAILED;
        }

        boolean valid = ValidationHandler.exists(object.getValue()) &&
                ValidationHandler.exists(object.getTimestamp()) &&
                validateGradeValue(object.getValue());
        return valid ? Response.Status.OK : Response.Status.PARTIAL_CONTENT;
    }

    @Override
    public Response.Status validateExistence(Grade object, List<Grade> list) {
        boolean exists = list.stream().
                anyMatch(f ->
                        f.getCourseId().equals(object.getCourseId()) &&
                        f.getStudentIndex().equals(object.getStudentIndex()) &&
                        f.getGradeId().equals(object.getGradeId()));
        return exists ? Response.Status.OK : Response.Status.NOT_FOUND;
    }

    static boolean validateGradeValue(Double value) {
        boolean valid = value >= 2.0 && value <= 5.0 && value % 0.5 == 0.0;
        return valid;
    }
}
