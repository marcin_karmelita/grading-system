import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by marcinkarmelita on 27/04/17.
 */
public interface ValidationHandler<T> {
    Response.Status validateContent(T object);
    Response.Status validateExistence(T object, List<T> list);

    static boolean exists(Object object) {
        return object != null;
    }
}
