import javax.ws.rs.core.Response;
import java.net.URI;

public abstract class ResponseProvider extends Response {

    // ok
    static public Response OK() {
        return OK(null);
    }
    static public Response OK(Object object) {
        return generic(Status.OK, object);
    }
    // not found
    static public Response notFound() {
        return notFound(null);
    }

    static public Response notFound(Object object) {
        return generic(Response.Status.NOT_FOUND, object);
    }

    // created
    static public Response created(Object object, String location) {
        return Response.status(Response.Status.CREATED)
                .location(URI.create(location))
                .build();
    }
    static public Response created(String location) {
        return created(null, location);
    }

    // forbidden
    static public Response forbidden(){
        return forbidden(null);
    }

    static public Response forbidden(Object object){
        return generic(Status.FORBIDDEN, object);
    }

    // partial content
    static public Response partialContent(){
        return partialContent(null);
    }

    static public Response partialContent(Object object){
        return generic(Status.PARTIAL_CONTENT, object);
    }

    // generic
    static public Response generic(Response.Status status) {
        return generic(status, null);
    }

    static public Response generic(Response.Status status, Object object) {
        return Response.status(status)
                .entity(object)
                .build();
    }
}
