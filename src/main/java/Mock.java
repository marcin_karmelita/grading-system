import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcinkarmelita on 20/04/17.
 */
public class Mock {
    private static Mock instance;
    public List<Student> students;
    public List<Grade> grades;
    public List<Course> courses;

    private Integer lastStudentIndex;
    private Integer lastCourseId;
    private Integer lastGradeId;

    private Mock() {
        students = Mock.students();
        grades = Mock.grades();
        courses = Mock.courses();
        lastCourseId = 179;
        lastStudentIndex = 116361;
        lastGradeId = 6;
    }

    public Integer getLastStudentIndex() {
        return ++lastStudentIndex;
    }

    public Integer getLastCourseId() {
        return ++lastCourseId;
    }

    public Integer getLastGradeId() {
        return ++lastGradeId;
    }

    public static Mock shared() {
        if (instance == null) {
            instance = new Mock();
        }
        return instance;
    }

    public static List<Student> students() {
        List<Student> students = new ArrayList<Student>();
        students.add(new Student(116360, "John", "Smith", (long) 1492723962));
        students.add(new Student(116361, "Jon", "Doe", (long) 1492723962));
        return students;
    }

    public static List<Grade> grades() {
        List<Grade> grades = new ArrayList<Grade>();
        grades.add(new Grade(116360, 177, 3.5, (long) 1492723962, 1));
        grades.add(new Grade(116360, 178, 4.5, (long) 1492723962, 2));
        grades.add(new Grade(116360, 179, 4.0, (long) 1492723962, 3));

        grades.add(new Grade(116361, 177, 3.0, (long) 1492723962, 4));
        grades.add(new Grade(116361, 178, 4.0, (long) 1492723962, 5));
        grades.add(new Grade(116361, 179, 4.5, (long) 1492723962, 6));

        return grades;
    }

    public static List<Course> courses() {
        List<Course> courses = new ArrayList<Course>();
        courses.add(new Course(177, "Java Programming Course", "Jon Doe, PhD"));
        courses.add(new Course(178, "C++ Programming Course", "Jon Doe, PhD"));
        courses.add(new Course(179, "Swift Programming Course", "Jon Doe, PhD"));
        return courses;
    }
}
