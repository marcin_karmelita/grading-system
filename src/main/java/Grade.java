import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by marcinkarmelita on 20/04/17.
 */
@XmlRootElement
public class Grade {

    private Integer studentIndex;
    private Integer courseId;
    private Double value;
    private Long timestamp;
    private Integer gradeId;

    public Grade() {

    }

    public Integer getGradeId() {
        return gradeId;
    }

    public void setGradeId(Integer gradeId) {
        this.gradeId = gradeId;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Grade(Integer studentIndex, Integer courseId, Double value, Long timestamp, Integer gradeId) {
        this.studentIndex = studentIndex;
        this.courseId = courseId;
        this.value = value;
        this.timestamp = timestamp;
        this.gradeId = gradeId;
    }

    public Integer getStudentIndex() {
        return studentIndex;
    }

    public void setStudentIndex(Integer studentIndex) {
        this.studentIndex = studentIndex;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public Double getValue() {
        return value;
    }

    public void setValue(Double value) {
        this.value = value;
    }

    public void update(Grade grade) {
        this.timestamp = grade.timestamp;
        this.value = grade.value;
    }
}
