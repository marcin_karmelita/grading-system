import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by marcinkarmelita on 20/04/17.
 */
@Path("/courses")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Courses implements Finder<Course> {
//    private List<Course> courses = Mock.courses();

    private ValidationHandler validationHandler = new CourseValidationHandler();

    @GET
    public Response getCourses() {
        return ResponseProvider.OK(Mock.shared().courses);
    }

    @GET
    @Path("{id}")
    public Response getCourses(@PathParam("id") Integer id) {
        return findById(id);
    }

    @POST
    public Response postCourse(Course course) {

        Response.Status status = validationHandler.validateContent(course);
        switch (status) {
            case OK:
                course.setId(Mock.shared().getLastCourseId());
                Mock.shared().courses.add(course);
                return ResponseProvider.created("/courses/" + course.getId().toString());
            default:
                return ResponseProvider.generic(status);
        }
    }

    @PUT
    @Path("{id}")
    public Response putCourse(Course course, @PathParam("id") Integer id) {
        switch (validationHandler.validateContent(course)) {
            case PARTIAL_CONTENT:
                return ResponseProvider.partialContent();
            case EXPECTATION_FAILED:
                return ResponseProvider.generic(Response.Status.EXPECTATION_FAILED);
        }

        course.setId(id);
        switch (validationHandler.validateExistence(course, Mock.shared().courses)) {
            case NOT_FOUND:
                return ResponseProvider.notFound();
        }

        Mock.shared().courses.stream()
                .filter(f -> f.getId().equals(id))
                .findFirst()
                .ifPresent(course1 -> course1.update(course));

        return ResponseProvider.OK();
    }

    @DELETE
    @Path("{id}")
    public Response deleteCourse(@PathParam("id") Integer id) {
        Response response = findById(id);
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            Mock.shared().courses.remove((response.getEntity()));
            return ResponseProvider.OK();
        }
        return response;
    }

    @Override
    public Response find(List<Course> list, Predicate<Course> predicate) {
        Optional<Course> temp = list.stream().filter(predicate).findFirst();
        if (temp.isPresent()) {
            return ResponseProvider.OK(temp.get());
        }
        return ResponseProvider.notFound();
    }

    private Response findById(Integer id) {
        return find(Mock.shared().courses, (f -> f.getId().equals(id)));
    }
}
