import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by marcinkarmelita on 20/04/17.
 */
@XmlRootElement
public class Student {
    private Integer index;
    private String firstName;
    private String lastName;
    private Long birthDate;

    public Long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Long birthDate) {
        this.birthDate = birthDate;
    }

//        private Courses courses;


    public Student(Integer index, String firstName, String lastName, Long birthDate) {
        this.index = index;
        this.firstName = firstName;
        this.lastName = lastName;
        this.birthDate = birthDate;
    }

    Student() {

    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getIndex() {
        return index;

    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void update(Student student) {
        this.firstName = student.firstName;
        this.lastName = student.lastName;
        this.birthDate = student.birthDate;
    }
}
