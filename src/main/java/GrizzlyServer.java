import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Created by marcinkarmelita on 07/04/17.
 */
public class GrizzlyServer {

    public static void main(String [] args) {
        URI baseUri = UriBuilder.fromUri("http://localhost/").port(9998).build();
        ResourceConfig config = new ResourceConfig(Students.class);
        config.registerClasses(Grades.class);
        config.registerClasses(Courses.class);
        GrizzlyHttpServerFactory.createHttpServer(baseUri, config);
    }
}
