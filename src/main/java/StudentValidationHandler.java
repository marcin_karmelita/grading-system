import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by marcinkarmelita on 27/04/17.
 */
public class StudentValidationHandler implements ValidationHandler<Student> {

    @Override
    public Response.Status validateContent(Student student) {
        if (student.getIndex() != null) {
            return Response.Status.EXPECTATION_FAILED;
        }
        if (ValidationHelper.validateName(student.getFirstName()) && ValidationHelper.validateName(student.getLastName()) && ValidationHelper.validateBirthDate(student.getBirthDate())) {
            return Response.Status.OK;
        }
        return Response.Status.PARTIAL_CONTENT;
    }

    @Override
    public Response.Status validateExistence(Student student, List<Student> list) {
        boolean exists = (list).stream().
                anyMatch(f -> f.getIndex().equals((student).getIndex()));
        if (exists) {
            return Response.Status.OK;
        }
        return Response.Status.NOT_FOUND;
    }
}
