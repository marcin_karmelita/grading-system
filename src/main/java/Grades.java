import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by marcinkarmelita on 20/04/17.
 */
@Path("/courses/{id}/grades")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class Grades implements Finder<Grade> {

//    private List<Grade> grades = Mock.grades();
    private ValidationHandler validationHandler = new GradeValidationHandler();

    @GET
    public Response getGrades(@PathParam("id") Integer id) {
        return findById(id);
    }

    @GET
    @Path("{index}")
    public Response getGrade(@PathParam("id") Integer id, @PathParam("index") Integer index) {
        return findBy(index, id);
    }

    @POST
    @Path("{index}")
    public Response postGrade(@PathParam("id") Integer id, @PathParam("index") Integer index, Grade grade) {
        // TODO Location ? where it should be created?
        Response.Status status = validationHandler.validateContent(grade);
        switch (status) {
            case OK:
                grade.setStudentIndex(index);
                grade.setCourseId(id);
                grade.setGradeId(Mock.shared().getLastGradeId());
                Mock.shared().grades.add(grade);
                return ResponseProvider.created("/courses/" + grade.getCourseId() + "/grades/" + grade.getStudentIndex() + "/" + grade.getGradeId());
            default:
                return ResponseProvider.generic(status);
        }
    }

    @PUT
    @Path("{index}/{gradeId}")
    public Response putGrade(@PathParam("id") Integer id, @PathParam("index") Integer index, @PathParam("gradeId") Integer gradeId, Grade grade) {
        switch (validationHandler.validateContent(grade)) {
            case PARTIAL_CONTENT:
                return ResponseProvider.partialContent();
            case EXPECTATION_FAILED:
                return ResponseProvider.generic(Response.Status.EXPECTATION_FAILED);
        }

        grade.setGradeId(gradeId);
        grade.setCourseId(id);
        grade.setStudentIndex(index);
        switch (validationHandler.validateExistence(grade, Mock.shared().grades)) {
            case NOT_FOUND:
                return ResponseProvider.notFound();
        }

        Mock.shared().grades.stream()
                .filter(f -> f.getStudentIndex().equals(index) && f.getCourseId().equals(id) && f.getGradeId().equals(gradeId))
                .findFirst()
                .ifPresent(grade1 -> grade1.update(grade));

        return ResponseProvider.OK();
    }

    @DELETE
    @Path("{index}/{gradeId}")
    public Response deleteGrade(@PathParam("id") Integer id, @PathParam("index") Integer index, @PathParam("gradeId") Integer gradeId, Grade grade) {

        Response response = findBy(index, id, gradeId);
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            ((List<Grade>)response.getEntity()).forEach(f -> Mock.shared().grades.remove(f));
            return ResponseProvider.OK();
        }
        return response;
    }

    @Override
    public Response find(List<Grade> list, Predicate<Grade> predicate) {
        List<Grade> temp = list.stream().filter(predicate).collect(Collectors.toList());
        if (!temp.isEmpty()) {
            return ResponseProvider.OK(temp);
        }
        return ResponseProvider.notFound();
    }

    private Response findBy(Integer index, Integer id) {
        return find(Mock.shared().grades, (f -> f.getStudentIndex().equals(index) && f.getCourseId().equals(id)));
    }

    private Response findBy(Integer index, Integer id, Integer gradeId) {
        return find(Mock.shared().grades, (f -> f.getStudentIndex().equals(index) && f.getCourseId().equals(id) && f.getGradeId().equals(gradeId)));
    }

    private Response findByIndex(Integer index) {
        return find(Mock.shared().grades, (f -> f.getStudentIndex().equals(index)));
    }

    private Response findById(Integer id) {
        return find(Mock.shared().grades, (f -> f.getCourseId().equals(id)));
    }
}
