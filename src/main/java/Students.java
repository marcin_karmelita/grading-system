import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

/**
 * Created by marcinkarmelita on 07/04/17.
 */
@Path("/students")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class Students implements Finder<Student> {

//    private List<Student> students = Mock.shared().students;
    private ValidationHandler validationHandler = new StudentValidationHandler();

    @GET
    public List<Student> getStudents() {
        return Mock.shared().students;
    }

    @GET
    @Path("{index}")
    public Response getStudent(@PathParam("index") Integer index) {
        return findByIndex(index);
    }

    @POST
    public Response postStudent(Student student) {

        Response.Status status = validationHandler.validateContent(student);
        switch (status) {
            case OK:
                student.setIndex(Mock.shared().getLastStudentIndex());
                Mock.shared().students.add(student);
                return ResponseProvider.created("/students/" + student.getIndex().toString());
                default:
                    return ResponseProvider.generic(status);
        }
    }

    @PUT
    @Path("{index}")
    public Response putStudent(Student student, @PathParam("index") Integer index) {
        switch (validationHandler.validateContent(student)) {
            case PARTIAL_CONTENT:
                return ResponseProvider.partialContent();
            case EXPECTATION_FAILED:
                return ResponseProvider.generic(Response.Status.EXPECTATION_FAILED);
        }

        student.setIndex(index);
        switch (validationHandler.validateExistence(student, Mock.shared().students)) {
            case NOT_FOUND:
                return ResponseProvider.notFound();
        }

        Mock.shared().students.stream()
                .filter(f -> f.getIndex().equals(index))
                .findFirst()
                .ifPresent(student1 -> student1.update(student));

        return ResponseProvider.OK();
    }

    @DELETE
    @Path("{index}")
    public Response deleteStudent(@PathParam("index") Integer index) {
        Response response = findByIndex(index);
        if (response.getStatus() == Response.Status.OK.getStatusCode()) {
            Mock.shared().students.remove(((Student)response.getEntity()));
            return ResponseProvider.OK();
        }
        return response;
    }

    // filtering
    private Optional<Student> findBy(Integer index) {
        return Mock.shared().students.stream()
                .filter(f -> f.getIndex().equals(index))
                .findFirst();
    }

    private Response findByIndex(Integer index) {
        return find(Mock.shared().students, (f -> f.getIndex().equals(index)));
    }

    @Override
    public Response find(List<Student> list, Predicate<Student> predicate) {
        Optional<Student> temp = list.stream().filter(predicate).findFirst();
        if (temp.isPresent()) {
            return ResponseProvider.OK(temp.get());
        }
        return ResponseProvider.notFound();
    }

}
