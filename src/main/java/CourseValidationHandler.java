import javax.ws.rs.core.Response;
import java.util.List;

/**
 * Created by marcinkarmelita on 27/04/17.
 */
public class CourseValidationHandler implements ValidationHandler<Course> {

    @Override
    public Response.Status validateContent(Course object) {
        if (ValidationHandler.exists(object.getId())) {
            return Response.Status.EXPECTATION_FAILED;
        }
        boolean valid = ValidationHandler.exists(object.getLecturer()) && ValidationHandler.exists(object.getName());
        return valid ? Response.Status.OK : Response.Status.PARTIAL_CONTENT;
    }

    @Override
    public Response.Status validateExistence(Course object, List<Course> list) {
        boolean exists = (list).stream().
                anyMatch(f -> f.getId().equals(object.getId()));
        return exists ? Response.Status.OK : Response.Status.NOT_FOUND;
    }
}
