import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by marcinkarmelita on 20/04/17.
 */

@XmlRootElement
public class Course {
    private Integer id;
    private String name;
    private String lecturer;

    public Course() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public Course(Integer id, String name, String lecturer) {

        this.id = id;
        this.name = name;
        this.lecturer = lecturer;
    }

    public void update(Course course) {
        this.lecturer = course.lecturer;
        this.name = course.name;
    }
}
