import javax.ws.rs.core.Response;
/**
 * Created by marcinkarmelita on 27/04/17.
 */
public abstract class ValidationHelper {

    public static Response.Status validate(Object object) {
        if (object.getClass() == Student.class) {
            return validateStudent((Student) object);
        }
        return Response.Status.OK;
    }

    static boolean validateName(String name) {
        return name != null && !name.isEmpty();
    }

    static boolean validateBirthDate(Long birthDate) {
        return birthDate != null;
    }

    private static Response.Status validateStudent(Student student) {
        if (ValidationHelper.validateName(student.getFirstName()) && ValidationHelper.validateName(student.getLastName()) && ValidationHelper.validateBirthDate(student.getBirthDate())) {
            return Response.Status.OK;
        }
        return Response.Status.PARTIAL_CONTENT;
    }
}
